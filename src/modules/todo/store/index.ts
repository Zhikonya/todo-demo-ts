/* tslint:disable:no-shadowed-variable */

import { ActionTree, GetterTree, Module, MutationTree } from 'vuex';
import store from '@/common/store';
import { ICommonState } from '@/common/store/interfaces';

export interface ITodoState {
  todos: string[];
}

const state: ITodoState = {
  todos: [],
};
const actions: ActionTree<ITodoState, ICommonState> = {
  setTodos: ({ commit }, payload) => commit('SET_TODOS', payload),
};
const mutations: MutationTree<ITodoState> = {
  SET_TODOS: (state: ITodoState, payload) => (state.todos = payload),
};
const getters: GetterTree<ITodoState, ICommonState> = {
  todos: (state: ITodoState) => state.todos,
};
const todoModule: Module<ITodoState, ICommonState> = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};

store.registerModule('todo', todoModule);
store.commit('SET_DETAIL_MODULES', { name: 'todo-tab', path: 'todo' }, { root: true });

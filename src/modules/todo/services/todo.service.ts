import Vue from 'vue';
import { ApiService } from '@/common/services/api.service';
export class TodoService extends ApiService {
  public static getInstance(): TodoService {
    if (TodoService.instance) {
      return TodoService.instance;
    }
    return (TodoService.instance = new TodoService());
  }
  protected static instance: TodoService;
  constructor() {
    super();
    TodoService.instance = this;
  }
  public loadTodos(id: number): Promise<any> {
    return this.axiosInstance.get('todos', { params: { userId: id } });
  }
}
export function TodoServicePlugin(vue: typeof Vue): void {
  vue.prototype.$todoService = TodoService.getInstance();
}

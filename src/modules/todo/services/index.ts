import Vue from 'vue';
import { TodoService, TodoServicePlugin } from '@/modules/todo/services/todo.service';
declare module 'vue/types/vue' {
  interface Vue {
    $todoService: TodoService;
  }
}
Vue.use(TodoServicePlugin);

import Vue from 'vue';
import './router';
import './services';
import './locales';
import './store';

Vue.component('todo-tab', () => import('./views/TodoTab.vue'));

import router from '@/common/router';

router.addRoutes([
  {
    path: '/todo/:id',
    component: () => import(/* webpackChunkName: "tdp" */ '../views/TodoPage.vue'),
  },
]);

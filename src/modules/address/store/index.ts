/* tslint:disable:no-shadowed-variable */

import { ActionTree, GetterTree, Module, MutationTree } from 'vuex';
import store from '@/common/store';
import { ICommonState } from '@/common/store/interfaces';
export interface IAddressState {
  showMap: boolean;
}
const state: IAddressState = {
  showMap: false,
};
const actions: ActionTree<IAddressState, ICommonState> = {
  toggleMap: ({ commit }) => commit('TOGGLE_MAP'),
};
const mutations: MutationTree<IAddressState> = {
  TOGGLE_MAP: (state: IAddressState) => (state.showMap = !state.showMap),
};
const getters: GetterTree<IAddressState, ICommonState> = {
  showMap: (state: IAddressState) => state.showMap,
};
const addressModule: Module<IAddressState, ICommonState> = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
store.registerModule('address', addressModule);
store.commit('SET_DETAIL_MODULES', { name: 'address-tab' }, { root: true });

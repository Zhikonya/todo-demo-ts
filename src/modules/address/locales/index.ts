import en from './en.json';
import i18n from '@/common/locales';
i18n.mergeLocaleMessage('en', { modules: { address: en } });

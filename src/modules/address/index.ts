import Vue from 'vue';
import './locales';
import './store';

Vue.component('address-tab', () => import('./views/AddressTab.vue'));

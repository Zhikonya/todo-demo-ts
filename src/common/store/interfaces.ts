export interface IUser {
  id: number;
  email: string;
  name: string;
  phone: string;
  username: string;
  website: string;
  selected?: boolean;
}
export interface IDetailModule {
  name: string;
  path?: string;
}
export interface ICommonState {
  users: IUser[];
  detailModules: IDetailModule[];
}

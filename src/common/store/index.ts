/* tslint:disable:no-shadowed-variable*/
import Vue from 'vue';
import Vuex, { ActionTree, GetterTree, MutationTree, StoreOptions } from 'vuex';
import { ICommonState, IUser } from '@/common/store/interfaces';

Vue.use(Vuex);

const state: ICommonState = {
  users: [],
  detailModules: [],
};
const actions: ActionTree<ICommonState, ICommonState> = {
  setUsers: ({ commit }, payload) => commit('SET_USERS', payload),
  selectUser: ({ commit }, payload) => commit('SET_SELECTED', payload),
};
const mutations: MutationTree<ICommonState> = {
  SET_USERS(state, payload): void {
    state.users = payload as IUser[];
  },
  SET_SELECTED(state, payload): void {
    state.users = state.users.map((user: IUser) => {
      user.selected = user.id === payload ? !user.selected : false;
      return user;
    });
  },
  SET_DETAIL_MODULES(state, payload) {
    state.detailModules.push(payload);
  },
};
const getters: GetterTree<ICommonState, ICommonState> = {
  users: (state: ICommonState) => state.users,
  selected: (state: ICommonState) => state.users.find((user: IUser) => user.selected),
  detailModules: (state: ICommonState) => state.detailModules,
};

const storeOptions: StoreOptions<ICommonState> = {
  state,
  actions,
  mutations,
  getters,
};
export default new Vuex.Store<ICommonState>(storeOptions);

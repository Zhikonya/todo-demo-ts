import Vue from 'vue';
import { ApiServicePlugin } from './api.service';

Vue.use(ApiServicePlugin);

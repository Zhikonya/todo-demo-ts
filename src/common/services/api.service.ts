import Vue from 'vue';
import axios, { AxiosInstance } from 'axios';

export class ApiService {
  // API base URL
  private static readonly BASE_URL: string = 'https://jsonplaceholder.typicode.com/';

  // API instance
  private static api: ApiService = new ApiService();

  // axios instance
  public axiosInstance: AxiosInstance;

  // constructor
  public constructor() {
    this.axiosInstance = axios.create({
      baseURL: ApiService.BASE_URL,
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      },
    });

    // response interceptor
    this.axiosInstance.interceptors.response.use(
      (response: any) => response,
      (error: any) => {
        if (error.response) {
          console.log('Error response: (data, status, headers)');
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log('Error', error.message);
        }
        console.log('Request config:');
        console.log(error.config);
        return Promise.reject(error);
      },
    );
  }

  public loadUsers(): Promise<any> {
    return this.axiosInstance.get('/users');
  }

  // getter returns API instance
  public static get $api(): ApiService {
    return ApiService.api;
  }
}

// plugin declaration
export function ApiServicePlugin(vue: typeof Vue): void {
  vue.prototype.$api = ApiService.$api;
}

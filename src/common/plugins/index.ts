import Vue from 'vue';
import VueWait from 'vue-wait';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSpinner,
  faUser,
  faTicketAlt,
  faExternalLinkAlt,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAngleRight,
  faAngleLeft,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(faSpinner, faUser, faTicketAlt, faExternalLinkAlt, faAngleRight, faAngleLeft, faAngleDoubleLeft, faAngleDoubleRight);

Vue.use(VueWait);

Vue.component('font-awesome-icon', FontAwesomeIcon);

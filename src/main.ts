import Vue, { CreateElement } from 'vue';
import VueWait from 'vue-wait';
import App from './App.vue';
import router from './common/router';
import store from './common/store';
import i18n from './common/locales';
import { ApiServicePlugin } from '@/common/services/api.service';

import './common/plugins';

// modules
import './modules';

Vue.use(ApiServicePlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  wait: new VueWait({}),
  render: (h: CreateElement) => h(App),
}).$mount('#app');

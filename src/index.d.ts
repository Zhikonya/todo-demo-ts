import Vue from 'vue';
import VueWait from 'vue-wait';
import { ApiService } from '@/common/services/api.service';

declare module 'vue/types/vue' {
  export interface VueConstructor {
    $wait: VueWait;
    $api: ApiService;
  }
  export interface Vue {
    $wait: VueWait;
    $api: ApiService;
  }
}

declare module 'vue/types/options' {
  export interface ComponentOptions<V extends Vue> {
    wait?: VueWait;
  }
}

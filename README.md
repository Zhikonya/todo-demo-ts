# todo-demo-ts
This is a simple user show project with data that uses a modular approach with independent paths and states

##### Project setup
```
npm install
```

##### Compiles and hot-reloads for development
```
npm run serve
```

##### Compiles and minifies for production
```
npm run build
```

##### Lints and fixes files
```
npm run lint
```

module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        'eslint:recommended',
        '@vue/typescript/recommended',
        '@vue/prettier',
        '@vue/prettier/@typescript-eslint',
    ],
    parserOptions: {
        ecmaVersion: 2020,
    },
    rules: {
        'no-debugger': 'off',
        semi: 'off', // disable the base rule as it can report incorrect errors
        '@typescript-eslint/semi': ['error'],
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: false }],
        indent: 'off',
        '@typescript-eslint/indent': ['error', 2],
        '@typescript-eslint/interface-name-prefix': 'off'
    },
};
